//#include "../include/fastexp.h"
#include <stdio.h>
#include <stdlib.h>
#include <gmp.h>
#include <time.h>
#include <sys/time.h>


//IDEAS: Maybe it got no sense to implement a binary tree when I can
//map any result to an index
//struct{
//	int exp;
//	mpz_t result;
//} dat;
//
//int compare(dat* a, dat* b){
//	return a->exp - b->exp;
//}
//dat* clone(dat* a){
//	dat* b = malloc(sizeof(dat));
//	mpz_set(b->result, a->result);
//	b->exp=a->exp;
//	return b;
//}
//
//void destroy(dat* a){
//	free(a);
//}
//int cutom_fexp(mpz_t result, mpz_t base, mpz_t exp, mpz_t modul){
//	
//
//}

mpz_t* results = NULL;
int longbits  = 0;

int init_fastexp(mpz_t base, mpz_t modul, int len){
	int i = 0;
	mpz_t temp;

	longbits = len;
	results = malloc (sizeof(mpz_t)* len);

	mpz_init_set(temp, base);
	mpz_init(results[0]);
	mpz_set(results[0], temp);

	for(i = 1; i < len; i++){
		mpz_init(results[i]);

		mpz_mul(temp, temp, temp);
		mpz_mod(temp, temp, modul);

		mpz_set(results[i], temp);

	}

	return 0;
}
int free_fastexp(){
	int i = 0;
	if (results == NULL)
		return 1;
	for(i = 0 ; i < longbits; i++){
		mpz_clear(results[i]);
	}
	free(results);
	results = NULL;
	return 1;
}


int fastexp(mpz_t result, mpz_t base, mpz_t exp, mpz_t modul){
	int i = 0; 
	mpz_t mask, temp;

	mpz_init_set_ui(mask, 1);
	mpz_init(temp);

	mpz_set_ui(result, 1);
	for(i = longbits - 1; i >= 0; i--){
		mpz_fdiv_q_2exp(temp, exp, i);
		mpz_and(temp, temp, mask);
		
		if(mpz_cmp(temp, mask) == 0) {
			mpz_mul(result, result, results[i]);
			mpz_mod(result, result, modul);
		}
	}
	mpz_clear(temp);
	mpz_clear(mask);
	return 0;
}

int cutom_exp(mpz_t result, mpz_t base, mpz_t exp, mpz_t modul){
	mpz_t mask, temp;
	int i, longbits;

	mpz_init_set_ui(mask, 1);
	mpz_set_ui(result, 1);
	mpz_init(temp);

	longbits = mpz_sizeinbase(exp, 2);
	for(i = longbits - 1; i >= 0; i--) {
	 
		mpz_mul(result, result, result);
		//mpz_mul_2exp(result, result, 2);
		mpz_mod(result, result, modul); // x = x² mod n
		 
		//Equivalente a un shift right
		mpz_fdiv_q_2exp(temp, exp, i);
		mpz_and(temp, temp, mask);          // temp = bit i de exponente
		
		if(mpz_cmp(temp, mask) == 0) {      // bit i == 1
			mpz_mul(result, result, base);
			mpz_mod(result, result, modul);
		}
	}

	mpz_clear(temp);
	mpz_clear(mask);
	return 0;
}

int main2(int argc, char** argv){
	mpz_t base, modul, exp, result;
	gmp_randstate_t state;

	gmp_randinit_default(state);
	gmp_randseed_ui(state, time(NULL));

	mpz_init(base);
	mpz_init(modul);
	mpz_init(exp);
	mpz_init(result);
	
	mpz_urandomb(base, state, atoi(argv[1]));
	mpz_urandomb(modul, state, atoi(argv[1]));
	mpz_urandomb(exp, state, atoi(argv[1]));

	init_fastexp(base, modul, atoi(argv[1]));
	cutom_exp(result, base, exp, modul);
	gmp_printf("%Zd\n\n", result);
	fastexp(result, base, exp, modul);
	gmp_printf("%Zd\n\n", result);
	free_fastexp();

	mpz_clear(base);
	mpz_clear(modul);
	mpz_clear(exp);
	mpz_clear(result);

	gmp_randclear(state);


}
int main(int argc, char** argv){
	//input values and controls
	int len = atoi(argv[1]);
	int rounds = atoi(argv[2]);
	int i = 0;
	//time vars
	struct timeval time1, time2;
	struct timeval* times_gmp,* times_cust,* times_fast;
	long sum_gmp = 0, sum_cust = 0, sum_fast = 0;
	//modular exp vars
	mpz_t base, modul, exp, result;
	gmp_randstate_t state;

	//alloc time values
	times_gmp = malloc(sizeof(struct timeval)*(rounds));
	times_cust = malloc(sizeof(struct timeval)*(rounds));
	times_fast = malloc(sizeof(struct timeval)*(rounds));

	//initialize the random seed
	gmp_randinit_default(state);
	gmp_randseed_ui(state, time(NULL));

	//initialize the number structures
	mpz_init(base);
	mpz_init(modul);
	mpz_init(exp);
	mpz_init(result);
	
	//generate the random number for the module and base
	mpz_urandomb(base, state, len);
	mpz_urandomb(modul, state, len);

	init_fastexp(base, modul, len);

	for(; i < rounds; ++i){
		//generate the multiple random numbers for the exponent
		mpz_urandomb(exp, state, len);

		gettimeofday(&time1, NULL);
		cutom_exp(result, base, exp, modul);
		gettimeofday(&time2, NULL);
		//gmp_printf("%Zd\n\n", result);

		(times_cust[i]).tv_usec = (time2.tv_sec - time1.tv_sec)*1000000 + time2.tv_usec - time1.tv_usec;
		(times_cust[i]).tv_sec = (times_cust[i]).tv_usec/1000000 ;
		sum_cust += (time2.tv_sec - time1.tv_sec)*1000000 + time2.tv_usec - time1.tv_usec;

		gettimeofday(&time1, NULL);
		fastexp(result, base, exp, modul);
		gettimeofday(&time2, NULL);
		//gmp_printf("%Zd\n\n", result);

		(times_fast[i]).tv_usec = (time2.tv_sec - time1.tv_sec)*1000000 + time2.tv_usec - time1.tv_usec;
		(times_fast[i]).tv_sec = (times_fast[i]).tv_usec/1000000 ;
		sum_fast += (time2.tv_sec - time1.tv_sec)*1000000 + time2.tv_usec - time1.tv_usec;

		gettimeofday(&time1, NULL);
		mpz_powm(result, base, exp, modul);
		gettimeofday(&time2, NULL);
		//gmp_printf("%Zd\n\n", result);
	
		(times_gmp[i]).tv_usec = (time2.tv_sec - time1.tv_sec)*1000000 + time2.tv_usec - time1.tv_usec;
		(times_gmp[i]).tv_sec = (times_gmp[i]).tv_usec/1000000 ;
		sum_gmp += (time2.tv_sec - time1.tv_sec)*1000000 + time2.tv_usec - time1.tv_usec;
		//printf("sec:%u usec:%lu\tsec:%u usec:%lu\tsec:%u usec:%lu\n", times_gmp[i].tv_sec, times_gmp[i].tv_usec, times_cust[i].tv_sec, times_cust[i].tv_usec, times_fast[i].tv_sec, times_fast[i].tv_usec);
	}
	//print resutls
	printf("%lu\t", sum_cust/rounds);
	printf("%lu\t", sum_fast/rounds);
	printf("%lu\n", sum_gmp/rounds);

	free_fastexp();

	//clear modular exp values
	mpz_clear(base);
	mpz_clear(modul);
	mpz_clear(exp);
	mpz_clear(result);
	gmp_randclear(state);

	//free time values
	free(times_gmp);
	free(times_cust);

	return 0;
}
