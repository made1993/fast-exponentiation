#ifndef _BTREE_H
#define _BTREE_H

#include <stdint.h>

#ifndef BTREE_FACTOR
#define BTREE_FACTOR (4)
#endif

#ifndef BTREE_CAPACITY
#define BTREE_CAPACITY 256
#endif

typedef struct _btree btree;
typedef struct _dat dat;

dat * dat_make(void * data, uint32_t size);
btree * btree_create(int (* compare)(dat * a, dat * b),
		     dat * (* clone)(dat * a),
		     void (* destroy)(dat * a));
void btree_destroy(btree * b);
unsigned int btree_insert(btree * b, dat * a);
unsigned int btree_delete(btree *b, dat * a);
unsigned int btree_find(btree * b, dat * a);
unsigned int btree_size(btree * b);

#endif
